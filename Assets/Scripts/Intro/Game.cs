﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public GameObject jamgame;
    public GameObject button;

    

    void Start()
    {
        jamgame.GetComponent<Animator>().SetBool("Click", false);
        button.GetComponent<Animator>().SetBool("Click", false);
    }

   
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!jamgame.GetComponent<Animator>().GetBool("Click"))
            {
                jamgame.GetComponent<Animator>().SetBool("Click", true);
                button.GetComponent<Animator>().SetBool("Click", true);
            }
            


        }
    }
    
}
