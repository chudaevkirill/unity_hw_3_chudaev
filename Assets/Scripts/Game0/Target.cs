using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace Game0
{

[RequireComponent(typeof(CoroutineAnimation))]
public class Target : MonoBehaviour
{
  public bool is_true_target;

  CoroutineAnimation coroutine_animation;

  void Start()
  {
  }

  public void SelfDestroy()
  {
    GetComponent<Animator>().SetTrigger("die");
  }

  public void Destory()
  {
    GameObject.Destroy(gameObject);
  }
}

}
