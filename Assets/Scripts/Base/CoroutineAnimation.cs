using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base
{

public class CoroutineAnimation : MonoBehaviour
{
  public enum Animation
  {
    SPAWN,
    DIE
  }

  IEnumerator current_animation;

  const float ANGLE = 90;

  public bool IsBusy
  {
    get {
      return current_animation != null;
    }
  }

  public void StartAnimation(Animation animation, float duration,  Action on_complete = null)
  {
    if(IsBusy)
      return;

    current_animation = DoAnimation(animation, duration, on_complete);
    StartCoroutine(current_animation);
  }

  Vector3 initial_position;
  Quaternion initial_rotation;
  Vector3 initial_scale;

  void Awake()
  {
    initial_position = transform.localPosition;
    initial_rotation = transform.localRotation;
    initial_scale = transform.localScale;
  }

  IEnumerator DoChangeTransform(float duration, Action<Vector3, Quaternion, Vector3, float> on_frame)
  {
    initial_position = transform.localPosition;
    initial_rotation = transform.localRotation;
    for(var initial_duration = duration; duration > 0; duration -= Time.deltaTime)
    {
      var t = (initial_duration - duration) / initial_duration;
      on_frame(initial_position, initial_rotation, initial_scale, t);
      yield return null;
    }
    on_frame(initial_position, initial_rotation, initial_scale, 1.0f);
  }

  IEnumerator Spawn(float duration)
  {
    var quaternion = Quaternion.AngleAxis(ANGLE, Vector3.up) * transform.rotation;
    yield return DoChangeTransform(duration, (position, rotation, scale, time) => {
      transform.localScale = Vector3.Lerp(Vector3.zero, scale, time);
      transform.localRotation = Quaternion.Lerp(quaternion, rotation, time);
    });
  }

  IEnumerator Die(float duration)
  {
    var initial_rotation = transform.rotation;
    var quaternion = Quaternion.AngleAxis(ANGLE, Vector3.up) * transform.rotation;
    yield return DoChangeTransform(duration, (position, rotation, scale, time) => {
      transform.localScale = Vector3.Lerp(scale, Vector3.zero, time);
      transform.localRotation = Quaternion.Lerp(rotation, quaternion, time);
    });
    transform.rotation = initial_rotation;
  }

  IEnumerator DoAnimation(Animation animation, float duration, Action on_complete = null)
  {
    switch(animation)
    {
      case Animation.SPAWN:
        yield return Spawn(duration);
        break;
      case Animation.DIE:
        yield return Die(duration);
        break;
    }

    on_complete?.Invoke();
    current_animation = null;
  }

  void Update()
  {
  }
}

}
