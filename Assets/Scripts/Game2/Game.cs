﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Game2
{

public class Game : MonoBehaviour
{
  public List<Unit> enemy;
  public Unit hero;
  Unit.Ability selected = null;

  public TargetMarker target_marker;

  public Camera game_camera;

  public UIAbilities ui_abilities;

  public Text hero_text_hp;

  public Animator enemy_turn;

  bool IsGameOver()
  {
    if(!hero.IsAlive())
      return true;

    bool have_alive_enemy = false;
    foreach(var unit in enemy)
      have_alive_enemy = have_alive_enemy || unit.IsAlive();

    return !have_alive_enemy;
  }

  IEnumerator DoUpdate()
  {
    while(!IsGameOver())
    {
      enemy_turn.SetBool("show", false);
      yield return HeroTurn(hero);

      enemy_turn.SetBool("show", true);
      yield return new WaitForSeconds(0.2f);
      foreach(var unit in enemy)
        yield return EnemyTurn(unit);

      yield return null;
    }

    yield return new WaitForSeconds(3.0f);

    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }

  void Start()
  {
    target_marker.unit = enemy[0];

    ui_abilities.Init(hero, (ability) => selected = ability);

    foreach(var unit in enemy)
      unit.LookAtTarget(hero);

    hero.LookAtTarget(enemy[0]);

    StartCoroutine(DoUpdate());
  }

  GameObject Hit(Vector3 hit_position)
  {
    RaycastHit hit;
    Ray ray = game_camera.ScreenPointToRay(hit_position);

    if (Physics.Raycast(ray, out hit)) {
      return hit.transform.gameObject;
    }

    return null;
  }

  IEnumerator UnitApplyAbility(Unit unit, Unit target, Unit.Ability ability)
  {
    Debug.Log($"--- {unit.name} apply {unit.abilities[0].name} to {target.name}");

    unit.UseAbility(ability);
    if(!string.IsNullOrEmpty(ability.animation_trigger))
      unit.StartAnimation(ability.animation_trigger);

    yield return new WaitForSeconds(1.0f);
    target.Hit(ability);

    if(!target.IsAlive())
      target.StartAnimation("die");

    yield return null;
  }

  IEnumerator HeroTurn(Unit hero)
  {
    ui_abilities.GetComponent<Animator>().SetBool("show", true);
    hero.OnTurnStart();
    while(true)
    {
      if(selected != null)
      {
        if(selected.target_type != Unit.Ability.TargetType.SELF && (target_marker.unit == null || !target_marker.unit.IsAlive()))
        {
          selected = null;
          continue;
        }

        ui_abilities.GetComponent<Animator>().SetBool("show", false);
        var target = target_marker.unit;
        if(selected.target_type == Unit.Ability.TargetType.SELF)
          target = hero;
        yield return UnitApplyAbility(hero, target, selected);
        selected = null;
        break;
      }

      if(!Input.GetMouseButtonDown(0))
      {
        yield return null;
        continue;
      }

      GameObject hit = Hit(Input.mousePosition);
      if(hit == null || hit == hero || hit.GetComponent<Unit>() == null)
      {
        yield return null;
        continue;
      }

      var unit = hit.GetComponent<Unit>();
      if(!unit.IsAlive())
      {
        yield return null;
        continue;
      }

      hero.LookAtTarget(unit);
      target_marker.unit = unit;
      yield return null;
    }
  }

  IEnumerator EnemyTurn(Unit unit)
  {
    if(!unit.IsAlive() || !hero.IsAlive())
      yield break;

    unit.OnTurnStart();
    unit.LookAtTarget(hero);
    yield return UnitApplyAbility(unit, hero, unit.abilities[0]);
  }

  void Update()
  {
    if(hero_text_hp == null)
      return;

    hero_text_hp.text = $"{hero.hp}/{hero.max_hp}";
  }
}

}
